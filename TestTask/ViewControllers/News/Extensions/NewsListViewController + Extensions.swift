//
//  NewsListViewController + Extensions.swift
//  TestTask
//
//  Created by Виктор Сирик on 17.03.2022.
//

import Foundation
import UIKit

extension NewsListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.dataArticle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.tag = indexPath.row
        cell.update(article: news.dataArticle[indexPath.row] )
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let articleVC = ArticleViewController(nibName: "ArticleViewController", bundle: nil)
        articleVC.urlPath = news.dataArticle[indexPath.row].articles.url
        navigationController?.pushViewController(articleVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == news.dataArticle.count - 1 && news.dataArticle.count < pagination.totalResults {
            pagination.nextPage += 1
            loadNews()
        }
    }
}

extension NewsListViewController: NewsCellDelegate{
    func checkSimilarArticles(index: Int) -> Bool {
        for item in savedNews {
            if item.articles.title == news.dataArticle[index].articles.title && item.articles.content == news.dataArticle[index].articles.content{ return false }
        }
        return true
    }
    
    func saveArticle(index: Int) {
        news.dataArticle[index].isSelectedCell = !news.dataArticle[index].isSelectedCell
        if news.dataArticle[index].isSelectedCell {
            if savedNews.isEmpty {
                savedNews.append(news.dataArticle[index])
                DatabaseManager.shared.saveData(data: savedNews)
            } else if checkSimilarArticles(index: index) {
                savedNews.append(news.dataArticle[index])
                DatabaseManager.shared.saveData(data: savedNews)
                }
        } else {
            
            for (savedIndex, item) in savedNews.enumerated() {
                if item.articles.title == news.dataArticle[index].articles.title && item.articles.content == news.dataArticle[index].articles.content{
                    savedNews.remove(at: savedIndex)
                    DatabaseManager.shared.saveData(data: savedNews)
                    break
                }
            }
        }
        newsTableView.reloadData()
    }
}

extension NewsListViewController: FilterNewsViewControllerDelegate{
    func dataFilter(category: Category?, country: Сountry?) {
        self.country = country
        self.category = category
        loadNews()
        newsTableView.reloadData()
    }
}

extension NewsListViewController: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        pagination = Pagination(nextPage: 1, pageSize: 10, totalResults: 0)
        news.dataArticle = []
        guard let findTitle = searchBar.text else { return }
        category = nil
        country = nil
        q = findTitle
        loadNews()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension String {
    init?(htmlEncodedString: String){
        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [ .documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }
        self.init(attributedString.string)
    }
}
