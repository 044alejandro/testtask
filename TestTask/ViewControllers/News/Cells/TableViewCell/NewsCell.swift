//
//  NewsCell.swift
//  TestTask
//
//  Created by Виктор Сирик on 20.03.2022.
//

import UIKit
import Kingfisher

protocol NewsCellDelegate {
    func saveArticle(index: Int)
}

class NewsCell: UITableViewCell {
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var saveArticleButton: UIButton!
    
    var delegate: NewsCellDelegate?
    
    func update(article: DataArticle) {
        let decodeTitle = String(htmlEncodedString: article.articles.title ?? "")
        let decodeDescription = String(htmlEncodedString: article.articles.description ?? "")
        let decodeAuthor = String(htmlEncodedString: article.articles.author ?? "")
        let decodeSource = String(htmlEncodedString: article.articles.source?.name ?? "")
        
        titleLabel.text = decodeTitle
        descriptionLabel.text = decodeDescription
        authorLabel.text = decodeAuthor
        sourceLabel.text = decodeSource
        
        guard let imageURL = article.articles.urlToImage else {return}
        loadImage(urla: imageURL, imageView: articleImage)
        
        if article.isSelectedCell {
            saveArticleButton.setImage(UIImage(systemName: "bookmark.fill"), for: .normal)
        } else {
            saveArticleButton.setImage(UIImage(systemName: "bookmark"), for: .normal)
        }
    }
    
    private func loadImage(urla: String, imageView: UIImageView) {
        let url = URL(string: urla)
        imageView.kf.setImage(with: url)
    }
    
    @IBAction func saveArticleAction(_ sender: Any) {
        delegate?.saveArticle(index: tag)
    }
}

