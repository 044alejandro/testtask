//
//  NewsListViewController.swift
//  TestTask
//
//  Created by Виктор Сирик on 17.03.2022.
//

import UIKit
import Alamofire

class NewsListViewController: UIViewController {

    @IBOutlet weak var findNewsSearchBar: UISearchBar!
    @IBOutlet weak var newsTableView: UITableView!
    
    let refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        return rc
    }()
    
    var news: DataCell = DataCell()
    var savedNews: [DataArticle] = []
    var q: String?
    var category: Category?
    var country: Сountry?
    var idSource: String?
    var pagination = Pagination(nextPage: 1, pageSize: 10, totalResults: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addItemsNavBar()
        newsTableView.refreshControl = refreshControl
        findNewsSearchBar.delegate = self
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
        loadNews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        savedNews = DatabaseManager.shared.getData() ?? []
        self.checkIsSelected()
        self.newsTableView.reloadData()
    }
    
    func addItemsNavBar(){
        let rightButton = UIBarButtonItem(image: UIImage(systemName: "bookmark"), style: .done, target: self, action: #selector(showSelectedArticles))
        let leftButtonSort = UIBarButtonItem(image: UIImage(systemName: "arrow.up.arrow.down"), style: .done, target: self, action: #selector(sortNews))
        let leftButtonFilter = UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .done, target: self, action: #selector(filterNews))

        self.navigationItem.title = "News"
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.leftBarButtonItems = [leftButtonSort, leftButtonFilter]
    }
    
    @objc func refresh(sender: UIRefreshControl){
        DispatchQueue.main.async {
            self.news.dataArticle = []
            self.loadNews()
        sender.endRefreshing()
        self.newsTableView.reloadData()
        }
    }
    
    @objc func showSelectedArticles(){
        let selectedArticlesVC = SelectedArticlesViewController(nibName: "SelectedArticlesViewController", bundle: nil)
        navigationController?.pushViewController(selectedArticlesVC, animated: true)
    }
    
    @objc func sortNews(){
        let alert = AlertManager().sortNewsAlert(news: news){ data in
            self.news.dataArticle = data
            self.newsTableView.reloadData()
        }
        present(alert, animated: true, completion: nil)
    }
    
    @objc func filterNews(){
        let filterVC = FilterNewsViewController(nibName: "FilterNewsViewController", bundle: nil)
        filterVC.delegate = self
        navigationController?.pushViewController(filterVC, animated: true)
    }
    
    func checkParameters() -> [String: Any] {
        let apiKey = "62e99e74e86d4d1b862f05f814ce18c2"
        var parameters: [String: Any] = [
             "pageSize" : pagination.pageSize, "page" : pagination.nextPage, "apiKey" : apiKey]
        if q != nil {
            parameters["q"] = q
        }
        if country != nil {
            parameters["country"] = country
        }
        if category != nil {
            parameters["category"] = category
        }
        if idSource != nil && category == nil && country == nil {
            parameters["sources"] = idSource
        } else if idSource == nil && category == nil && country == nil && q == nil{
            parameters["q"] = "news"
        }
        return parameters
    }
    
    func checkIsSelected() {
        for (index, newsItem) in (news.dataArticle).enumerated() {
            for selectedItem in savedNews {
                if selectedItem.articles.description == newsItem.articles.description &&  selectedItem.articles.title == newsItem.articles.title{
                    news.dataArticle[index].isSelectedCell = true
                } else {
                    news.dataArticle[index].isSelectedCell = false
                }
            }
        }
    }
    
    func loadNews() {
        let url = "https://newsapi.org/v2/top-headlines"
        let parameters: [String: Any] = checkParameters()
        RestData().decodeArray(urlPath: url, parameters: parameters, completionHandler:{ (data: News?, error: String?) -> Void in
            
            guard error == nil else {
                print(error)
                return
            }
            guard let data = data else {
                print("data is nil")
                return
            }
            guard data.status == "ok" else {
             let alert = AlertManager().notFoundDataAlert()
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            if data.articles?.count == 0 {
                let alert = AlertManager().notFoundDataAlert()
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.pagination.totalResults = data.totalResults ?? 0
            for item in data.articles ?? [] {
                let article = DataArticle(articles: item, isSelectedCell: false)
                self.news.dataArticle.append(article)
            }
            self.newsTableView.reloadData()
        })
    }
}

