//
//  SourceListViewController.swift
//  TestTask
//
//  Created by Виктор Сирик on 19.03.2022.
//

import UIKit

class SourceListViewController: UIViewController {
    @IBOutlet weak var sourcesTableView: UITableView!
    
    var category: Category?
    var country: Сountry?
    var sources: [SourceModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        navigationController?.navigationBar.tintColor = UIColor.white
        sourcesTableView.register(UINib(nibName: "SourceCell", bundle: nil), forCellReuseIdentifier: "SourceCell")
        
        sourcesTableView.delegate = self
        sourcesTableView.dataSource = self
    }
    
    func loadData() {
        let url = "https://newsapi.org/v2/top-headlines/sources"
        let apiKey = "62e99e74e86d4d1b862f05f814ce18c2"
        var parameters: [String: Any] = ["apiKey" : apiKey]

        if country != nil { parameters["country"] = country }
        if category != nil { parameters["category"] = category }
        
        RestData().decodeArray(urlPath: url, parameters: parameters, completionHandler: {
            (data: SourceResult?, error: String?) -> Void in
            
            guard error == nil else {
                print(error)
                return
            }
            guard let data = data else {
                print("data is nil")
                return
            }
            
            if data.sources?.count == 0 {
                let alert = AlertManager().notFoundDataAlert()
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.sources = data.sources ?? []
            self.sourcesTableView.reloadData()
        })
    }
}
