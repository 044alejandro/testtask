//
//  SourceCell.swift
//  TestTask
//
//  Created by Виктор Сирик on 19.03.2022.
//

import UIKit

class SourceCell: UITableViewCell {
    @IBOutlet weak var sourceTitleLabel: UILabel!
    
    func update(source: SourceModel) {
        sourceTitleLabel.text = source.name
    }
}
