//
//  SourceListViewController + Extensions.swift
//  TestTask
//
//  Created by Виктор Сирик on 19.03.2022.
//

import Foundation
import UIKit

extension SourceListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: sourcesTableView.frame.width, height: 40))
        let label = UILabel(frame: view.frame)

        label.text = "Sources"
        label.textColor = .white
        label.backgroundColor = UIColor.systemBlue
        label.font = UIFont.systemFont(ofSize: 28)
        label.textAlignment = .center
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SourceCell", for: indexPath) as! SourceCell
        cell.update(source: sources[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsVC = NewsListViewController(nibName: "NewsListViewController", bundle: nil)
        newsVC.idSource = sources[indexPath.row].id
        self.navigationController?.pushViewController(newsVC, animated: true)
    }
}
