//
//  ArticleViewController.swift
//  TestTask
//
//  Created by Виктор Сирик on 19.03.2022.
//

import UIKit
import WebKit

class ArticleViewController: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var urlPath: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
        guard let url = URL(string: urlPath ?? "") else { return }
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    func loadWebView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
}
