//
//  SelectedArticlesViewController + Extensions.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import Foundation
import UIKit

extension SelectedArticlesViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        savedArticles = DatabaseManager.shared.getData() ?? []
        return savedArticles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.update(article: savedArticles[indexPath.row])
        cell.delegate = self
        cell.tag = indexPath.row
        return cell
    }
}

extension SelectedArticlesViewController: NewsCellDelegate{
    func saveArticle(index: Int) {
        savedArticles.remove(at: index)
        DatabaseManager.shared.saveData(data: savedArticles)
        savedArticlesTableView.reloadData()
    }
}
