//
//  SelectedArticlesViewController.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import UIKit

class SelectedArticlesViewController: UIViewController {
    
    @IBOutlet weak var savedArticlesTableView: UITableView!
    
    var savedArticles: [DataArticle] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        savedArticlesTableView.delegate = self
        savedArticlesTableView.dataSource = self
        
        savedArticlesTableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
        savedArticles = DatabaseManager().getData() ?? []
        savedArticlesTableView.reloadData()
    }

}
