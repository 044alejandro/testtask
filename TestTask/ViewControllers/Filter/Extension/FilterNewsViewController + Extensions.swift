//
//  FilterNewsViewController + Extensions.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import Foundation
import UIKit

extension FilterNewsViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countriesPickerView {
            return countries.count
        } else { return categories.count }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countriesPickerView {
            return countries[row].rawValue
        } else {
            return categories[row].rawValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countriesPickerView {
            countryTextField.text = countries[row].rawValue
            country = countries[row]
            countryTextField.resignFirstResponder()
        } else {
            categoryTextField.text = categories[row].rawValue
            category = categories[row]
            categoryTextField.resignFirstResponder()
        }
    }
}

extension FilterNewsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == countryTextField {
            categoryTextField.becomeFirstResponder()
        } else{
            categoryTextField.resignFirstResponder()
        }
        return true
    }
}

