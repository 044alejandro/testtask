//
//  FilterNewsViewController.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import UIKit

protocol FilterNewsViewControllerDelegate{
    func dataFilter(category: Category?, country: Сountry?)
}

class FilterNewsViewController: UIViewController {
    
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var filterBySourceButton: UIButton!
    @IBOutlet weak var filterBySelectedButton: UIButton!
    
    var countriesPickerView = UIPickerView()
    var categoriesPickerView = UIPickerView()
    var countries: [Сountry] = [.noCountry, .ae, .ar, .at, .au, .be, .bg, .br, .ca, .ch, .cn, .co, .cu, .cz, .de, .eg, .fr, .gb, .gr, .hk, .hu, .id, .ie, .il, .in, .it, .jp, .kr, .lt, .lv, .ma, .mx, .my, .ng, .nl, .no, .nz, .ph, .pl, .pt, .ro, .rs, .ru, .sa, .se, .sg, .si, .sk, .th, .tr, .tw, .ua, .us, .ve, .za]
    var categories: [Category] = [.noCategory, .business, .entertainment, .general, .technology, .health, .sports, .science]
    var category: Category?
    var country: Сountry?
    var delegate: FilterNewsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryTextField.delegate = self
        countryTextField.delegate = self

        countriesPickerView.delegate = self
        countriesPickerView.dataSource = self
        
        categoriesPickerView.delegate = self
        categoriesPickerView.dataSource = self
        
        countryTextField.inputView = countriesPickerView
        categoryTextField.inputView = categoriesPickerView
        
        filterBySourceButton.layer.cornerRadius = filterBySourceButton.frame.height / 2
        filterBySelectedButton.layer.cornerRadius = filterBySelectedButton.frame.height / 2
    }

    @IBAction func filterBySourceAction(_ sender: Any) {
        let selectSourceVC = SourceListViewController(nibName: "SourceListViewController", bundle: nil)
        selectSourceVC.country = country
        selectSourceVC.category = category
        self.navigationController?.pushViewController(selectSourceVC, animated: true)
    }
    
    @IBAction func filterBySelected(_ sender: Any) {
        delegate?.dataFilter(category: category, country: country)
        navigationController?.popViewController(animated: true)
    }
}
