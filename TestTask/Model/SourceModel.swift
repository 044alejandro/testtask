//
//  SourceModel.swift
//  TestTask
//
//  Created by Виктор Сирик on 19.03.2022.
//

import Foundation

// MARK: - SourceResult
struct SourceResult: Codable{
    let status: String?
    let sources: [SourceModel]?
}

// MARK: - SourceModel
struct SourceModel: Codable{
    let id, name, sourceDescription: String?
    let url: String?
    let category, language, country: String?
}
