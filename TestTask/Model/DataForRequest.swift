//
//  DataForRequest.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import Foundation

enum Сountry: String {
    case noCountry = ""
    case ae = "UAE"
    case ar = "Argentina"
    case at = "Austria"
    case au = "Australia"
    case be = "Belgium"
    case bg = "Bulgaria"
    case br = "Brazil"
    case ca = "Canada"
    case ch = "Switzerland"
    case cn = "China"
    case co = "Colombia"
    case cu = "Cuba"
    case cz = "Czech Republic"
    case de = "Germany"
    case eg = "Egypt"
    case fr = "France"
    case gb = "United Kingdom"
    case gr = "Greece"
    case hk = "Hong Kong"
    case hu = "Hungary"
    case id = "Indonesia"
    case ie = "Ireland"
    case il = "Israel"
    case `in` = "India"
    case it = "Italy"
    case jp = "Japan"
    case kr = "Korea, Republic"
    case lt = "Lithuania"
    case lv = "Latvia"
    case ma = "Morocco"
    case mx = "Mexico"
    case my = "Malaysia"
    case ng = "Nigeria"
    case nl = "Netherlands"
    case no = "Norway"
    case nz = "New Zealand"
    case ph = "Philippines"
    case pl = "Poland"
    case pt = "Portugal"
    case ro = "Romania"
    case rs = "Serbia"
    case ru = "Russian Federation"
    case sa = "Saudi Arabia"
    case se = "Sweden"
    case sg = "Singapore"
    case si = "Slovenia"
    case sk = "Slovakia"
    case th = "Thailand"
    case tr = "Turkey"
    case tw = "Taiwan"
    case ua = "Ukraine"
    case us = "United States"
    case ve = "Venezuela"
    case za = "South Africa"
}

enum Category: String{
    case noCategory = ""
    case business = "Business"
    case entertainment = "Entertainment"
    case general = "General"
    case health = "Health"
    case science = "Science"
    case sports = "Sports"
    case technology = "Technology"
}

struct Pagination{
    var nextPage: Int
    var pageSize: Int
    var totalResults: Int
}
