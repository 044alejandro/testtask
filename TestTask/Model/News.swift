//
//  News.swift
//  TestTask
//
//  Created by Виктор Сирик on 17.03.2022.
//

import Foundation

// MARK: - DataCell
struct DataCell {
    var dataArticle: [DataArticle] = []
}

// MARK: - DataArticle
struct DataArticle {
    var articles: Article
    var isSelectedCell = false
}

// MARK: - News
struct News: Codable{
    var status: String?
    var totalResults: Int?
    var articles: [Article]?
}

// MARK: - Article
struct Article: Codable{
    var source: Source?
    var author, title, description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
}

// MARK: - Source
struct Source: Codable {
    var id, name: String?
}
