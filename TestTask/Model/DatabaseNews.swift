//
//  DatabaseNews.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import Foundation
import RealmSwift

// MARK: - DataCell
class DataCellDatabase: Object{
    @Persisted var dataArticle: List<NewsDatabase>
}

// MARK: - NewsDatabase
class NewsDatabase: Object{
    @Persisted var isSelectedCell: Bool
    @Persisted var articles: ArticleDatabase?
}

// MARK: - ArticleDatabase
class ArticleDatabase: Object{
    @Persisted var source: SourceDatabase?
    @Persisted var author: String
    @Persisted var title: String
    @Persisted var descriptionInfo: String
    @Persisted var url: String
    @Persisted var urlToImage: String
    @Persisted var publishedAt: String
    @Persisted var content: String
}

// MARK: - SourceDatabase
class SourceDatabase: Object {
    @Persisted var id: String
    @Persisted var name: String
}
