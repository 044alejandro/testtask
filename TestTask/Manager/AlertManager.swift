//
//  AlertManager.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import Foundation
import UIKit

class AlertManager {
    func sortNewsAlert(news: DataCell, completionHandler: @escaping([DataArticle]) -> Void)  -> UIAlertController {
        let alert = UIAlertController(title: "", message: "Do you want to sort the news?", preferredStyle: .actionSheet)
        let latest = UIAlertAction(title: "sort by latest news", style: .default){ _ in
            completionHandler( news.dataArticle.sorted(by: { $0.articles.publishedAt ?? "" < $1.articles.publishedAt ?? ""}))
        }
        let oldest = UIAlertAction(title: "sort by oldest news", style: .default){ _ in
            completionHandler( news.dataArticle.sorted(by: { $0.articles.publishedAt ?? "" > $1.articles.publishedAt ?? ""}))
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(latest)
        alert.addAction(oldest)
        alert.addAction(cancel)
        return alert
    }
    
    func notFoundDataAlert()  -> UIAlertController {
        let alert = UIAlertController(title: "Sorry", message: "Nothing was found for your search", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cancel)
        return alert
    }
   
}
