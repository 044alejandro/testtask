//
//  NetworkManager.swift
//  TestTask
//
//  Created by Виктор Сирик on 17.03.2022.
//

import Foundation
import Alamofire

class NetworkManager {

    static let shared = NetworkManager()
    
    func loadData(urlPath: String,
                  method: HTTPMethod,
                  parameters: [String: Any]? = nil,
                  completionHandler: @escaping(Data?, String?) -> Void) {
        
        AF.request(urlPath,
                   method: method,
                   parameters: parameters).response {
            request in
            switch request.result {
            case .success(let data):
                completionHandler(data, nil)
            case .failure(let error) :
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
}
