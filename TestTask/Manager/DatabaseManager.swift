//
//  DatabaseManager.swift
//  TestTask
//
//  Created by Виктор Сирик on 18.03.2022.
//

import Foundation
import RealmSwift

class DatabaseManager {
    
    static let shared = DatabaseManager()
    private let realm = try? Realm()
    
    func saveData(data: [DataArticle]) {
        let transformToDb = transformToDatabaseModel(data: data)
        do{
            try realm?.write({
                realm?.add(transformToDb)
            })
        } catch(let error){
            print(error.localizedDescription)
        }
    }
    
    func transformToDatabaseModel(data: [DataArticle]) -> DataCellDatabase {
        let databaseListArticle: List<NewsDatabase> = List<NewsDatabase>()
        let databaseNews = DataCellDatabase()
        for item in data {
            let itemData = ArticleDatabase()
            itemData.author = item.articles.author ?? ""
            itemData.title = item.articles.title ?? ""
            itemData.descriptionInfo = item.articles.description ?? ""
            itemData.url = item.articles.url ?? ""
            itemData.urlToImage = item.articles.urlToImage ?? ""
            itemData.publishedAt = item.articles.publishedAt ?? ""
            itemData.content = item.articles.content ?? ""
            itemData.source?.id = item.articles.source?.id ?? ""
            itemData.source?.name = item.articles.source?.name ?? ""
            let itemResult = NewsDatabase()
            itemResult.articles = itemData
            itemResult.isSelectedCell = item.isSelectedCell
            databaseListArticle.append(itemResult)
        }
        databaseNews.dataArticle = databaseListArticle
        return databaseNews
    }
    
    func getData() -> [DataArticle]? {
        guard let newsDatabase = realm?.objects(DataCellDatabase.self).last else { return nil }
        return transformDataModel(databaseData: newsDatabase)
    }
    
    func transformDataModel(databaseData: DataCellDatabase) -> [DataArticle] {
        var articleArray: [DataArticle] = []
        
        for item in databaseData.dataArticle {
            var itemData = DataArticle(articles: Article())
            itemData.articles.author = item.articles?.author
            itemData.articles.title = item.articles?.title
            itemData.articles.description = item.articles?.descriptionInfo
            itemData.articles.url = item.articles?.url
            itemData.articles.urlToImage = item.articles?.urlToImage
            itemData.articles.publishedAt = item.articles?.publishedAt
            itemData.articles.content = item.articles?.content
            itemData.articles.source?.id = item.articles?.source?.id
            itemData.articles.source?.name = item.articles?.source?.name
            itemData.isSelectedCell = item.isSelectedCell
            articleArray.append(itemData)
        }
        return articleArray
    }
}


