//
//  RestData.swift
//  TestTask
//
//  Created by Виктор Сирик on 17.03.2022.
//

import Foundation

class RestData {
    func decodeArray<T: Codable>(urlPath: String, parameters: [String: Any]? = nil, completionHandler: @escaping(T?, String?) -> Void) {
        NetworkManager.shared.loadData(urlPath: urlPath, method: .get, parameters: parameters){
            data, error in
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            guard let result = data else {
                completionHandler(nil, nil)
                return
            }
            
            do{
                let decoder = JSONDecoder()
                let response = try decoder.decode(T.self, from: result)
                completionHandler(response, nil)
            } catch let resError as NSError{
                completionHandler(nil, resError.localizedDescription)
            }
        }
    }
}

